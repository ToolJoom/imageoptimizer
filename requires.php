<?php
/**
 * Image optimizer.
 *
 * @package ImageOptimizer
 * @author ToolJoom <contact@tooljoom.com>
 */

require_once __DIR__ . '/src/Exception/ConfigurationException.php';
require_once __DIR__ . '/src/Client/ClientInterface.php';
require_once __DIR__ . '/src/Client/AbstractClient.php';
require_once __DIR__ . '/src/Client/Curl.php';
require_once __DIR__ . '/src/Parser/ParserInterface.php';
require_once __DIR__ . '/src/Parser/AbstractParser.php';
require_once __DIR__ . '/src/Parser/JSON.php';
require_once __DIR__ . '/src/Response/Response.php';
require_once __DIR__ . '/src/ImageOptimizer.php';
