# ImageOptimizer

[![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT)

Optimize images using the free '**[reSmush.it](https://resmush.it/)**' service.

## Installation

### Composer

Run:

```sh
composer require tooljoom/imageoptimizer
```

### Manually

Include the following file:

```php
require_once '/path/to/ImageOptimizer/requires.php';
```

## Usage

### Method 1

```php
// Initialize the library.
$imageOptimizer = new \ToolJoom\ImageOptimizer\ImageOptimizer();

// Either provide a local image...
$image = '/path/to/image.png';

// ... or provide a remote image.
$image = 'http://path.to/image.png';

// The path to save the optimized image.
$saveAs = '/path/to/optimized_image.png';

// Make the call to the 'reSmush.it' service and save the image locally.
$result = $imageOptimizer->optimizeAndRetrieve($image, $saveAs);
```

The above method will return the path/filename of the optimized image on success or `false` on failure.

In case of failure, the error code and description can be retrieve using the `getDebugData()` method:

```php
// Make the call to the 'reSmush.it' service and save the image locally.
$result = $imageOptimizer->optimizeAndRetrieve($image, $saveAs);

if (false === $result) {
    $debugData = $imageOptimizer->getDebugData();
    
    echo $debugData['method']; // Returns 'optimize' or 'retrieve', depending in which step the error occured
    echo $debugData['errorCode'];
    echo $debugData['errorDescription'];
}
```

### Method 2

**Step 1) Get optimized image details**

```php
// Initialize the library.
$imageOptimizer = new \ToolJoom\ImageOptimizer\ImageOptimizer();

// Either provide a local image...
$image = '/path/to/image.png';

// ... or a remote image.
$image = 'http://path.to/image.png';

// Make the call to the 'reSmush.it' service.
$response = $imageOptimizer->optimize($image);
```

Response information can be retrieved using the following methods:

##### $response->getIsSuccessful();

Returns a boolean indicating if the call was successful or not.

##### $response->getImageOriginal();

Returns the URL of the original image on success or `null` on failure.

##### $response->getImageOptimized();

Returns the URL of the optimized image on success or `null` on failure.

##### $response->getSizeOriginal();

Returns the size of the original image on success or `null` on failure.

##### $response->getSizeOptimized();

Returns the size of the optimized image on success or `null` on failure.

##### $response->getExpirationDate();

Returns the expiration date of the optimized image URL on success or `null` on failure.

##### $response->getErrorCode();

Returns the error code on failure or `null` on success.

##### $response->getErrorDescription();

Returns the error description on failure or `null` on success.

**Step 2) Retrieve the optimized image**

```php
// The path to save the optimized image.
$saveAs = '/path/to/optimized_image.png';

// Retrieve the optimized image.
if ($response->getIsSuccessful()) {
    $result = $imageOptimizer->retrieve($response->getImageOptimized(), $saveAs);
}
```

The above method will return the path/filename of the optimized image on success or `false` on failure.

In case of failure, the error code and description can be retrieve using the `getDebugData()` method:

```php
// Retrieve the optimized image.
if ($response->getIsSuccessful()) {
    $result = $imageOptimizer->retrieve($response->getImageOptimized(), $saveAs);

    if (false === $result) {
        $debugData = $imageOptimizer->getDebugData();

        echo $debugData['errorCode'];
        echo $debugData['errorDescription'];
    }
}
```

## Overriding the default configuration

It is possible to override the default configuration by providing a custom configuration array to the library constructor.

```php
$customConfiguration = [
    'client' => [
        'timeout' => 10
    ],
    'image'  => [
        'quality => 100
    ]
];

// Initialize the library with (partially) custom configuration.
$imageOptimizer = new \ToolJoom\ImageOptimizer\ImageOptimizer($customConfiguration);
``` 

The default configuration array is found in the `config/config.php` file.

## Notes

* To replace the original image with the optimized one, use the same path/filename *(applies to local images only)*.

        $image  = '/path/to/image.png';
        $result = $imageOptimizer->optimizeAndRetrieve($image, $image);

    By default, a backup with the extension `.bak` will be created next to the original image
    *(for example `/path/to/image.png.bak`)*. In order to skip backup creation, add a third parameter with the value
    `false` to the `optimizeAndRetrieve()` method. The same applies to the `retrieve()` method as well.

        $image  = '/path/to/image.png';
        $result = $imageOptimizer->optimizeAndRetrieve($image, $image, false);

* Depending on the configuration, a prefix and/or a suffix can be added automatically to the destination image filename.

    **Adding a prefix**

        $customConfiguration = [
            'image'  => [
                'prefix => 'prefix_'
            ]
        ];
        
        $saveAs = '/path/to/image.png';

    The resulting filename will be `/path/to/prefix_image.png`.
    
    **Adding a suffix**
    
        $customConfiguration = [
            'image'  => [
                'suffix => '_suffix'
            ]
        ];
     
        $saveAs = '/path/to/image.png';
    
    The resulting filename will be `/path/to/image_suffix.png`.
    
    **Adding both prefix and suffix**
        
        $customConfiguration = [
            'image'  => [
                'prefix => 'prefix_',
                'suffix => '_suffix'
            ]
        ];
     
        $saveAs = '/path/to/image.png';
    
    The resulting filename will be `/path/to/prefix_image_suffix.png`.



## License

Copyright © 2020 ToolJoom

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation
files (the “Software”), to deal in the Software without restriction, including without limitation the rights to use,
copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the
Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
