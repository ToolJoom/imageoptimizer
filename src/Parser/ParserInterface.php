<?php
/**
 * Image optimizer.
 *
 * @package ImageOptimizer
 * @author ToolJoom <contact@tooljoom.com>
 */

namespace ToolJoom\ImageOptimizer\Parser;

use ToolJoom\ImageOptimizer\Response\Response;

/**
 * Interface to be implemented by all library parsers.
 *
 * @since 1.0.0
 */
interface ParserInterface
{
    /**
     * Parse the provided response.
     *
     * @since 1.0.0
     * @access public
     * @param array $response
     * @return Response
     */
    public function parseResponse(array $response): Response;
}
