<?php
/**
 * Image optimizer.
 *
 * @package ImageOptimizer
 * @author ToolJoom <contact@tooljoom.com>
 */

namespace ToolJoom\ImageOptimizer\Parser;

use ToolJoom\ImageOptimizer\Response\Response;

/**
 * JSON response parser.
 *
 * @since 1.0.0
 */
final class JSON extends AbstractParser
{
    /**
     * Parse the provided response.
     *
     * @since 1.0.0
     * @access public
     * @param array $response
     * @return Response
     */
    public function parseResponse(array $response): Response
    {
        $output = new Response();

        if ($response['status']) {
            $responseObject = json_decode($response['response']);

            if (isset($responseObject->error)) {
                $output
                    ->setIsSuccessful(false)
                    ->setErrorCode($responseObject->error)
                    ->setErrorDescription($responseObject->error_long);
            } else {
                $output
                    ->setIsSuccessful(true)
                    ->setImageOriginal($responseObject->src)
                    ->setImageOptimized($responseObject->dest)
                    ->setSizeOriginal($responseObject->src_size)
                    ->setSizeOptimized($responseObject->dest_size)
                    ->setExpirationDate($responseObject->expires);
            }
        } else {
            $output
                ->setIsSuccessful(false)
                ->setErrorCode($response['error']['code'])
                ->setErrorDescription($response['error']['description']);
        }

        return $output;
    }
}
