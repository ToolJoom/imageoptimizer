<?php
/**
 * Image optimizer.
 *
 * @package ImageOptimizer
 * @author ToolJoom <contact@tooljoom.com>
 */

namespace ToolJoom\ImageOptimizer\Parser;

/**
 * Abstract class to be extended by all library parsers.
 *
 * @since 1.0.0
 */
abstract class AbstractParser implements ParserInterface
{

}
