<?php
/**
 * Image optimizer.
 *
 * @package ImageOptimizer
 * @author ToolJoom <contact@tooljoom.com>
 */

namespace ToolJoom\ImageOptimizer;

use ToolJoom\ImageOptimizer\Client\ClientInterface;
use ToolJoom\ImageOptimizer\Exception\ConfigurationException;
use ToolJoom\ImageOptimizer\Parser\ParserInterface;
use ToolJoom\ImageOptimizer\Response\Response;
use \CURLFile;

/**
 * Image optimizer.
 *
 * @since 1.0.0
 */
final class ImageOptimizer
{
    /**
     * Array holding the library configuration.
     *
     * @since 1.0.0
     * @access private
     * @var array
     */
    private $config = [];

    /**
     * The client to use for making calls to the image optimizing service.
     *
     * @since 1.0.0
     * @access private
     * @var ClientInterface
     */
    private $client = null;

    /**
     * The parser to use for extracting information from the image optimizing service response.
     *
     * @since 1.0.0
     * @access private
     * @var ParserInterface
     */
    private $parser = null;

    /**
     * The service URL to send requests for image optimization.
     *
     * @since 1.0.0
     * @access private
     * @var string
     */
    private $serviceURL = '';

    /**
     * Array holding the debug data in case of an error.
     *
     * @since 1.0.2
     * @access private
     * @var array
     */
    private $debugData = [];

    /**
     * Class constructor.
     *
     * @since 1.00
     * @access public
     * @param array $config Default value: <i>empty array</i>
     * @throws ConfigurationException
     */
    public function __construct($config = [])
    {
        $this->config = $this->loadConfiguration($config);
    }

    /**
     * Set the debug data in case of an error.
     *
     * @since 1.0.2
     * @access private
     * @param string $method
     * @param int $code
     * @param string $description
     * @return ImageOptimizer
     */
    private function setDebugData(string $method, int $code, string $description): ImageOptimizer
    {
        $this->debugData = [
            'method'           => $method,
            'errorCode'        => $code,
            'errorDescription' => $description
        ];

        return $this;
    }

    /**
     * Get the debug data in case of an error.
     *
     * @since 1.0.2
     * @access public
     * @return array
     */
    public function getDebugData(): array
    {
        return $this->debugData;
    }

    /**
     * Get optimized image details by making a call to the image optimizing service.
     *
     * @since 1.0.0
     * @access public
     * @param string $image
     * @return Response
     */
    public function optimize(string $image): Response
    {
        $this->cleanDebugData();

        $requestData = (is_file($image) ? $this->buildRequestLocal($image) : $this->buildRequestRemote($image));
        $response    = $this->client->sendRequest($requestData, $this->serviceURL, $this->config['client']['timeout']);

        return $this->parser->parseResponse($response);
    }

    /**
     * Save the optimized image locally.
     * Returns the optimized image path/filename on success or "false" on error.
     *
     * @since 1.0.0
     * @access public
     * @param string $optimizedImage
     * @param string $saveAs
     * @param bool $backupOriginal Default value: <b>true</b>
     * @return bool|string
     */
    public function retrieve(string $optimizedImage, string $saveAs, bool $backupOriginal = true)
    {
        $this->cleanDebugData();

        $output   = false;
        $proceed  = true;
        $pathInfo = pathinfo($saveAs);
        $filename = $pathInfo['dirname']
            . DIRECTORY_SEPARATOR
            . preg_replace("/[^[:alnum:]-_.]/u", '', $this->config['image']['prefix'])
            . $pathInfo['filename']
            . preg_replace("/[^[:alnum:]-_.]/u", '', $this->config['image']['suffix'])
            . '.'
            . $pathInfo['extension'];

        $pathInfo = pathinfo($filename);

        if (is_file($filename)) {
            if ($backupOriginal && !rename($filename, $filename . '.bak')) {
                $proceed = false;
            }
        } else {
            if (!is_dir($pathInfo['dirname'])) {
                if (!mkdir($pathInfo['dirname'], 0755, true)) {
                    $proceed = false;
                }
            }
        }

        if ($proceed) {
            $response = $this->client->retrieveImage($optimizedImage, $this->config['client']['timeout']);

            if ($response['status'] && !empty($response['response'])) {
                $fp = fopen($filename, 'w');

                if (false !== $fp) {
                    $fw = fwrite($fp, $response['response']);

                    if (false !== $fw) {
                        $output = $filename;
                    }

                    unset($fw);

                    fclose($fp);
                }
            } else {
                $this->setDebugData('retrieve', $response['error']['code'], $response['error']['description']);
            }
        }

        return $output;
    }

    /**
     * Get optimized image and save it locally.
     * Returns the optimized image path/filename on success or "false" on error.
     *
     * @since 1.0.0
     * @access public
     * @param string $image
     * @param string $saveAs
     * @param bool $backupOriginal Default value: <b>true</b>
     * @return bool|string
     */
    public function optimizeAndRetrieve(string $image, string $saveAs, bool $backupOriginal = true)
    {
        $output   = false;
        $optimize = $this->optimize($image);

        if ($optimize->getIsSuccessful()) {
            $output = $this->retrieve($optimize->getImageOptimized(), $saveAs, $backupOriginal);
        } else {
            $this->setDebugData('optimize', $optimize->getErrorCode(), $optimize->getErrorDescription());
        }

        return $output;
    }

    /**
     * Clean the debug data.
     *
     * @since 1.0.2
     * @access private
     * @return ImageOptimizer
     */
    private function cleanDebugData()
    {
        $this->debugData = [];

        return $this;
    }

    /**
     * Load the default library configuration.
     *
     * @since 1.0.0
     * @access private
     * @param array $config
     * @return array
     * @throws ConfigurationException
     */
    private function loadConfiguration(array $config): array
    {
        $filename = realpath(__DIR__ . '/../') . '/config/config.php';

        if (is_file($filename)) {
            $output = include $filename;

            foreach ($config as $key => $value) {
                if (isset($output[$key])) {
                    foreach ($value as $subKey => $subValue) {
                        if (isset($output[$key][$subKey])) {
                            $output[$key][$subKey] = $subValue;
                        }
                    }
                }
            }

            if (!in_array($output['service']['protocol'], ['http', 'https'], true)) {
                throw new ConfigurationException('Invalid service protocol provided.', 20, 'service/protocol', $output['service']['protocol']);
            }

            if (class_exists($output['client']['library'])) {
                $clientClass = new $output['client']['library']();

                if ($clientClass instanceof ClientInterface) {
                    $this->client = $clientClass;
                } else {
                    throw new ConfigurationException('Client library does not implement ClientInterface.', 30, 'client/library', $output['client']['library']);
                }
            } else {
                throw new ConfigurationException('Invalid client library provided.', 20, 'client/library', $output['client']['library']);
            }

            if (class_exists($output['client']['parser'])) {
                $parserClass = new $output['client']['parser']();

                if ($parserClass instanceof ParserInterface) {
                    $this->parser = $parserClass;
                } else {
                    throw new ConfigurationException('Parser library does not implement ParserInterface.', 30, 'client/parser', $output['client']['parser']);
                }
            } else {
                throw new ConfigurationException('Invalid parser library provided.', 20, 'client/parser', $output['client']['parser']);
            }

            if (!is_int($output['image']['quality']) || $output['image']['quality'] < 1 || $output['image']['quality'] > 100) {
                throw new ConfigurationException('Invalid image quality provided.', 20, 'image/quality', $output['image']['quality']);
            }

            $this->serviceURL = sprintf('%s://%s', $output['service']['protocol'], $output['service']['endpoint']);
        } else {
            throw new ConfigurationException('Default configuration file missing.', 1, '', '');
        }

        return $output;
    }

    /**
     * Build the request to optimize a local image.
     *
     * @since 1.0.0
     * @access private
     * @param string $image
     * @return array
     */
    private function buildRequestLocal(string $image): array
    {
        $data = [
            'files' => new CURLFile($image)
        ];

        return $this->buildRequest($data);
    }

    /**
     * Build the request to optimize a remote image.
     *
     * @since 1.0.0
     * @access private
     * @param string $image
     * @return array
     */
    private function buildRequestRemote(string $image): array
    {
        $data = [
            'img' => $image
        ];

        return $this->buildRequest($data);
    }

    /**
     * Build the request to optimize an image.
     *
     * @since 1.0.0
     * @access private
     * @param array $data
     * @return array
     */
    private function buildRequest(array $data): array
    {
        return array_merge(
            $data,
            [
                'qlty' => $this->config['image']['quality'],
                'exif' => (true === $this->config['image']['keepExifData'] ? 'true' : 'false')
            ]
        );
    }
}
