<?php
/**
 * Image optimizer.
 *
 * @package ImageOptimizer
 * @author ToolJoom <contact@tooljoom.com>
 */

namespace ToolJoom\ImageOptimizer\Client;

/**
 * Abstract class to be extended by all library clients.
 *
 * @since 1.0.0
 */
abstract class AbstractClient implements ClientInterface
{

}
