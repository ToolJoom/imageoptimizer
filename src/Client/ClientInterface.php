<?php
/**
 * Image optimizer.
 *
 * @package ImageOptimizer
 * @author ToolJoom <contact@tooljoom.com>
 */

namespace ToolJoom\ImageOptimizer\Client;

/**
 * Interface to be implemented by all library clients.
 *
 * @since 1.0.0
 */
interface ClientInterface
{
    /**
     * Send the request to the image optimizing service.
     *
     * @since 1.0.0
     * @access public
     * @param array $data
     * @param string $url
     * @param int $timeout
     * @return array
     */
    public function sendRequest(array $data, string $url, int $timeout): array;

    /**
     * Retrieve the optimized image.
     *
     * @since 1.0.0
     * @access public
     * @param string $image
     * @param int $timeout
     * @return array
     */
    public function retrieveImage(string $image, int $timeout): array;
}
