<?php
/**
 * Image optimizer.
 *
 * @package ImageOptimizer
 * @author ToolJoom <contact@tooljoom.com>
 */

namespace ToolJoom\ImageOptimizer\Client;

/**
 * CURL client.
 *
 * @since 1.0.0
 */
final class Curl extends AbstractClient
{
    /**
     * Send the request to the image optimizing service.
     *
     * @since 1.0.0
     * @access public
     * @param array $data
     * @param string $url
     * @param int $timeout
     * @return array
     */
    public function sendRequest(array $data, string $url, int $timeout): array
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        $response = curl_exec($ch);

        if ($response !== false) {
            $output = [
                'status'   => true,
                'response' => $response
            ];
        } else {
            $output = [
                'status' => false,
                'error'  => [
                    'code'        => curl_errno($ch),
                    'description' => curl_error($ch)
                ]
            ];
        }

        curl_close($ch);

        return $output;
    }

    /**
     * Retrieve the optimized image.
     *
     * @since 1.0.0
     * @access public
     * @param string $image
     * @param int $timeout
     * @return array
     */
    public function retrieveImage(string $image, int $timeout): array
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $image);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT ,0);
        curl_setopt($ch, CURLOPT_TIMEOUT, $timeout);

        $response = curl_exec($ch);

        if ($response !== false) {
            $output = [
                'status'   => true,
                'response' => $response
            ];
        } else {
            $output = [
                'status' => false,
                'error'  => [
                    'code'        => curl_error($ch),
                    'description' => curl_error($ch)
                ]
            ];
        }

        curl_close($ch);

        return $output;
    }
}
