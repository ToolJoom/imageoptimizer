<?php
/**
 * Image optimizer.
 *
 * @package ImageOptimizer
 * @author ToolJoom <contact@tooljoom.com>
 */

namespace ToolJoom\ImageOptimizer\Response;

use \DateTime;

/**
 * Image optimizing service response.
 *
 * @since 1.0.0
 */
final class Response
{
    /**
     * The status of the request.
     *
     * @since 1.0.0
     * @access private
     * @var bool
     */
    private $isSuccessful = null;

    /**
     * The URL of the original image.
     *
     * @since 1.0.0
     * @access private
     * @var null|string
     */
    private $imageOriginal = null;

    /**
     * The URL of the optimized image.
     *
     * @since 1.0.0
     * @access private
     * @var null|string
     */
    private $imageOptimized = null;

    /**
     * The size of the original image.
     *
     * @since 1.0.0
     * @access private
     * @var null|float
     */
    private $sizeOriginal = null;

    /**
     * The size of the optimized image.
     *
     * @since 1.0.0
     * @access private
     * @var null|float
     */
    private $sizeOptimized = null;

    /**
     * The expiration date of the remote optimized image.
     *
     * @since 1.0.0
     * @access private
     * @var null|DateTime
     */
    private $expirationDate = null;

    /**
     * The error code.
     *
     * @since 1.0.0
     * @access private
     * @var null|int
     */
    private $errorCode = null;

    /**
     * The error description.
     *
     * @since 1.0.0
     * @access private
     * @var null|string
     */
    private $errorDescription = null;

    /**
     * Get the status of the request.
     *
     * @since 1.0.0
     * @access public
     * @return bool
     */
    public function getIsSuccessful(): bool
    {
        return $this->isSuccessful;
    }

    /**
     * Set the status of the request.
     *
     * @since 1.0.0
     * @access public
     * @param bool $isSuccessful
     * @return Response
     */
    public function setIsSuccessful(bool $isSuccessful): Response
    {
        $this->isSuccessful = $isSuccessful;

        return $this;
    }

    /**
     * Get the URL of the original image.
     *
     * @since 1.0.0
     * @access public
     * @return string
     */
    public function getImageOriginal(): string
    {
        return $this->imageOriginal;
    }

    /**
     * Set the URL of the original image.
     *
     * @since 1.0.0
     * @access public
     * @param string $imageOriginal
     * @return Response
     */
    public function setImageOriginal(string $imageOriginal): Response
    {
        $this->imageOriginal = $imageOriginal;

        return $this;
    }

    /**
     * Get the URL of the optimized image.
     *
     * @since 1.0.0
     * @access public
     * @return string
     */
    public function getImageOptimized(): string
    {
        return $this->imageOptimized;
    }

    /**
     * Set the URL of the optimized image.
     *
     * @since 1.0.0
     * @access public
     * @param string $imageOptimized
     * @return Response
     */
    public function setImageOptimized(string $imageOptimized): Response
    {
        $this->imageOptimized = $imageOptimized;

        return $this;
    }

    /**
     * Get the size of the original image.
     *
     * @since 1.0.0
     * @access public
     * @return float
     */
    public function getSizeOriginal(): float
    {
        return $this->sizeOriginal;
    }

    /**
     * Set the size of the original image.
     *
     * @since 1.0.0
     * @access public
     * @param float $sizeOriginal
     * @return Response
     */
    public function setSizeOriginal(float $sizeOriginal): Response
    {
        $this->sizeOriginal = $sizeOriginal;

        return $this;
    }

    /**
     * Get the size of the optimized image.
     *
     * @since 1.0.0
     * @access public
     * @return float
     */
    public function getSizeOptimized(): float
    {
        return $this->sizeOptimized;
    }

    /**
     * Set the size of the optimized image.
     *
     * @since 1.0.0
     * @access public
     * @param float $sizeOptimized
     * @return Response
     */
    public function setSizeOptimized(float $sizeOptimized): Response
    {
        $this->sizeOptimized = $sizeOptimized;

        return $this;
    }

    /**
     * Get the expiration date of the remote optimized image.
     *
     * @since 1.0.0
     * @access public
     * @return DateTime
     */
    public function getExpirationDate(): DateTime
    {
        return $this->expirationDate;
    }

    /**
     * Set the expiration date of the remote optimized image.
     *
     * @since 1.0.0
     * @access public
     * @param string $expirationDate Date format: 'D, d M Y H:i:s O'
     * @return Response
     */
    public function setExpirationDate(string $expirationDate): Response
    {
        $format = 'D, d M Y H:i:s O';
        $date   = DateTime::createFromFormat($format, $expirationDate);

        if (false === $date || $date->format($format) !== $expirationDate) {
            $date = DateTime::createFromFormat($format, date($format, strtotime('+1 hour')));
        }

        $this->expirationDate = $date;

        return $this;
    }

    /**
     * Get the error code.
     *
     * @since 1.0.0
     * @access public
     * @return int
     */
    public function getErrorCode(): int
    {
        return $this->errorCode;
    }

    /**
     * Set the error code.
     *
     * @since 1.0.0
     * @access public
     * @param int $errorCode
     * @return Response
     */
    public function setErrorCode(int $errorCode): Response
    {
        $this->errorCode = $errorCode;

        return $this;
    }

    /**
     * Get the error description.
     *
     * @since 1.0.0
     * @access public
     * @return string
     */
    public function getErrorDescription(): string
    {
        return $this->errorDescription;
    }

    /**
     * Set the error description.
     *
     * @since 1.0.0
     * @access public
     * @param string $errorDescription
     * @return Response
     */
    public function setErrorDescription(string $errorDescription): Response
    {
        $this->errorDescription = $errorDescription;

        return $this;
    }
}
