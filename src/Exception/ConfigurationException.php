<?php
/**
 * Image optimizer.
 *
 * @package ImageOptimizer
 * @author ToolJoom <contact@tooljoom.com>
 */

namespace ToolJoom\ImageOptimizer\Exception;

use \Exception;

/**
 * Configuration exception class.
 *
 * @since 1.0.0
 */
final class ConfigurationException extends Exception
{
    /**
     * The name of the parameter.
     *
     * @since 1.0.0
     * @access private
     * @var string
     */
    private $parameterName = null;

    /**
     * The value of the parameter.
     *
     * @since 1.0.0
     * @access private
     * @var string
     */
    private $parameterValue = null;

    /**
     * Class constructor.
     *
     * @since 1.0.0
     * @access public
     * @param string $message
     * @param int $code
     * @param string $parameterName
     * @param string $parameterValue
     */
    public function __construct(string $message, int $code, string $parameterName, string $parameterValue)
    {
        // Call to parent constructor.
        parent::__construct($message, $code, null);

        // Set exception variables.
        $this->parameterName  = $parameterName;
        $this->parameterValue = $parameterValue;
    }

    /**
     * Get the name of the parameter.
     *
     * @since 1.0.0
     * @access public
     * @return string
     */
    public function getParameterName(): string
    {
        return $this->parameterName;
    }

    /**
     * Get the value of the parameter.
     *
     * @since 1.0.0
     * @access public
     * @return string
     */
    public function getParameterValue(): string
    {
        return $this->parameterValue;
    }
}
