<?php
/**
 * Image optimizer.
 *
 * @package ImageOptimizer
 * @author ToolJoom <contact@tooljoom.com>
 */

return [
    /**
     * Image optimizing service related data.
     *
     * @since 1.0.0
     * @var array
     */
    'service' => [
        /**
         * The HTTP protocol of the service.
         * Valid values: <b>http</b>>, <b>https</b>
         * Default value: <b>https</b>
         *
         * @since 1.0.0
         * @var string
         */
        'protocol' => 'https',

        /**
         * The endpoint of the service.
         * Default value: <b>api.resmush.it/ws.php</b>
         *
         * @since 1.0.0
         * @var string
         */
        'endpoint' => 'api.resmush.it/ws.php'
    ],

    /**
     * Client related data.
     *
     * @since 1.0.0
     * @var array
     */
    'client' => [
        /**
         * The class that will be used as the client library.
         * Class must implement the ClientInterface in order to be valid.
         *
         * @since 1.0.0
         * @var string
         */
        'library' => '\ToolJoom\ImageOptimizer\Client\Curl',

        /**
         * The class that will be used to parse the response of the service.
         * Class must implement the ParserInterface in order to be valid.
         *
         * @since 1.0.0
         * @var string
         */
        'parser' => '\ToolJoom\ImageOptimizer\Parser\JSON',

        /**
         * The maximum time in seconds the client should wait for a response.
         * Default value: <b>5</b>
         *
         * @since 1.0.0
         * @var int
         */
        'timeout' => 5
    ],

    /**
     * Optimized image related data.
     *
     * @since 1.0.0
     * @var array
     */
    'image' => [
        /**
         * The quality of the optimized image, with 1 being the worst quality and 100 the best.
         * Valid values: 1-100
         * Default value: <b>90</b>
         *
         * @since 1.0.0
         * @var int
         */
        'quality' => 90,

        /**
         * Indicator if EXIF data should be kept in the optimized image.
         * Default value: <b>false</b>
         *
         * @since 1.0.0
         * @var bool
         */
        'keepExifData' => false,

        /**
         * A prefix to add to the filename of the optimized image, if it is saved locally.
         * Default value: <i>empty string</i>
         *
         * @since 1.0.0
         * @var string
         */
        'prefix' => '',

        /**
         * A suffix to add to the filename of the optimized image, if it is saved locally.
         * Default value: <i>empty string</i>
         *
         * @since 1.0.0
         * @var string
         */
        'suffix' => ''
    ]
];
